const USER_URL = 'https://catappapi.herokuapp.com/users';

const KITTENS_URL = 'http://catappapi.herokuapp.com/cats';

const catMap = new Map();
catMap.set( 21, 'images/cat-21.jpg'  );
catMap.set( 33, 'images/cat-33.jpg'  );
catMap.set( 45, 'images/cat-45.jpg'  );


/**
 * 
 * @param {*} items 
 */
function buildDom( catArray ){
    
    const imgArray = catArray.map(  ( cat, index)  =>{
        let img = '';
        if( index === 0 ){
            img += `<div class="carousel-item active">`;
        }else {
            img += `<div class="carousel-item">`;
        }
        
        img += `<img src="${cat.imageUrl}" alt="${cat.name}" class="d-block w-100"/></div>`;
        return img;
    });

    //console.log( imgArray.join('') );      
    document.getElementById('innerCarousel').innerHTML =  imgArray.join('');

}


/**
 * response is  Promise 
 * 
 * 
 * @param {*} userId 
 */
async function loadCarousel(userId){
    
    const response = await fetch( `${USER_URL}/${userId}` );
    
    const user = await response.json();

    const catPromises =  user.cats.map(  async catId => {
        const catResponse = await fetch( `${KITTENS_URL}/${catId}` );
        const cat = await catResponse.json();
        cat.id = catId;
        cat.imageUrl = catMap.get(catId);
        return cat;
    });

    Promise.all(catPromises).then( buildDom );   
    
}


loadCarousel('123');