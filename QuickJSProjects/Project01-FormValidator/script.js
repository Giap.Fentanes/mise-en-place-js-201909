
/**
 * Create a const variable for the next elements. Get the elements by thier id
 * form,  username, email, password, password2
 * 
 */

/**
 * Show input error message 
 * To the parent elment of the input, append the class error.
 * Get the small element with a query Selector.
 * To the small item, use the function  innerText to put the message.
 * 
 * @param {any} input   HTML Element: The input html element,
 * @param {string} message String:  The error message to display
 */
function showError( ){
}

/**
 * Show success outline
 * Th the parent elment of the input, append the class success.
 * @param {any} input HTML: Element: The input html element
 */
function showSuccess(){       
}


/**
 * The function returns  true or false to validate an emai is correct.
 * @param {string} email  The string value to evaluate if its a valid email.
 * @returns {boolean}     The true or false value, depending of the evaluation    
 *  
 */
function isValidEmail(){
}


// Event listeners

/**
 * After execute e.preventDefault() , statr to evaluate each field
 * If the input value is empty (value === '') execute showError on input
 * otherwise execute showSuccess on input
 * 
 * ################################################################################
 * Instructions
 * 1) In the username input validate
 * 1.1 ) if username is empty send error message "Username is requiered"
 * 1.2 ) if username has content, send showSuccess 
 *  
 * 2) In the email input validate 
 * 2.1 ) If email is empty send error message "Username is requiered"
 * 2.2 ) If email is not valid, end error message "Email is not valid"
 * 2.3 ) if email has content and is valid, sen showSuccess
 * 
 * 3) In the password input validate
 * 3.1 ) If password is empty send error message "Pasword is requiered"
 * 3.2 ) If password has content, send showSuccess
 * 
 * 4) In the password2 input validate
 * 4.1 ) If password2 is empty send error message "Confirm password is required"
 * 4.2 ) If password2 has content, send showSuccess
 * 
 * ################################################################################
 * 
 */
form.addEventListener( 'submit', function( e ){
    e.preventDefault();
    


});

